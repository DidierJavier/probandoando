package org.example;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Double> matriz = new ArrayList<>();
        matriz.add(0,  -5.0);
        matriz.add(1, 3.0);
        matriz.add(2, 8.0);
        matriz.add(3, -2.0);
        matriz.add(4, -7.0);
        matriz.add(5, 4.0);
        ArrayList<Double> copia = matriz;
        for (int i = 0; i < matriz.size(); i++) {
            for (int j = i + 1; j < copia.size(); j++) {
                if(matriz.get(i) > copia.get(j)){
                    double valorMatriz = matriz.get(i);
                    double valorCopia = copia.get(j);
                    matriz.set(i, valorCopia);
                    copia.set(j, valorMatriz);
                    matriz = copia;
                }
            }
        }
        System.out.print("Orden ascendente = ");
        System.out.println(matriz);
        for (int i = 0; i < matriz.size(); i++) {
            for (int j = i + 1; j < copia.size(); j++) {
                if(matriz.get(i) < copia.get(j)){
                    double valorMatriz = matriz.get(i);
                    double valorCopia = copia.get(j);
                    matriz.set(i, valorCopia);
                    copia.set(j, valorMatriz);
                    matriz = copia;
                }
            }
        }
        System.out.print("Orden descendente = ");
        System.out.println(matriz);
    }
}